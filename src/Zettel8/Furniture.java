package Zettel8;

public interface Furniture {
    String getElement();//woraus is den möbel getan
    Boolean setElement(String element);//Zettel8.Test
    public String geElement();
}
class Table implements Furniture {

    public String getElement() {
      return "wood";
    }

    @Override
    public Boolean setElement(String element) {
        return null;
    }

    @Override
    public String geElement() {
        return null;
    }
}

class Chair implements Furniture {

    @Override
    public String getElement() {
        return "leather";
    }
    @Override
    public String geElement(){
        return "leather";
    }

    @Override
    public Boolean setElement(String element) {
        return null;
    }



}

class Desk extends Table {

}

class Office extends Room {
    Chair chair;
    Desk desk;
    Furniture furniture[];
    Office(Desk desk, Chair chair, Furniture furniture){
        this.furniture = new Furniture[1];
        this.chair = chair;
        this.desk = desk;
        this.furniture[0] = furniture;

    }
}
class Room {
    String name = "E60";
    Furniture[] furniture = new Furniture[3];

}

class Test {
    public static void main(String[] args) {
        Desk desk = new Desk();
        Office office = new Office(new Desk(),new Chair(), new Chair());
        System.out.println(desk.getElement());

    }
}

