

import java.io.Serializable;

public class GenericMethods {

	public static <T> T selectRandom(T v1, T v2) {
		if (Math.random() < 0.5) {
			return v1;
		} else {
			return v2;
		}
	}

	public static void main(String[] args) {
		int v1 = selectRandom(43, 56);
		String v2 = selectRandom("hallo", "dibo");
		// String v3 = selectRandom(43, "hallo"); // illegal
		Object obj = selectRandom(43, "hallo");
		Comparable cmp = selectRandom(43,  "hallo");
		Serializable ser = selectRandom(43,  "hallo");

	}

}
