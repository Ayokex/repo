package Zettel8;

import java.util.Iterator;

class MultiSet<T> implements Iterable<T> {
	@Override
	T Iterator;
	public Iterator<T> iterator() {
		return new T;
	}
	void add(T element){

	}
	int count(T element){}
}


class Elem<T> {
	T value;
	Elem<T> next;

	public Elem(T value, Elem<T> next) {
		super();
		this.value = value;
		this.next = next;
	}

	public T getValue() {
		return value;
	}

	public Elem<T> getNext() {
		return next;
	}
}

class ListIterator<T> implements Iterator<T> {

	Elem<T> current;

	ListIterator(List<T> s) {
		current = s.first;
	}

	@Override
	public boolean hasNext() {
		return current != null;
	}

	@Override
	public T next() {
		T result = current.getValue();
		current = current.getNext();
		return result;
	}

}

public class List<T> implements Iterable<T> {
	Elem<T> first = null;
	int size = 0;

	public void add(T value) {
		first = new Elem<T>(value, first);
		size++;
	}

	public int getSize() {
		return size;
	}

	public T get(int index) {
		Elem<T> current = first;
		for (int i= 0; i<index; i++) {
			current = current.getNext();
		}
		return current.getValue();
	}

	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(this);
	}

	// Testprogramm
	public static void main(String[] args) {
		List<String> list = new List<String>();
		list.add("hallo");
		list.add("dibo");
		
		Iterator<String> iter = list.iterator();
		while (iter.hasNext()) {
			String v = iter.next();
			System.out.println(v);
		}
		
		for (String v : list) {
			System.out.println(v);
		}
		
		for (int i=0; i<list.getSize(); i++) {
			System.out.println(list.get(i));
		}
	}

}
