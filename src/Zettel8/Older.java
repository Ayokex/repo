public interface Older<T> {
	public boolean isOlder(T other);
}
