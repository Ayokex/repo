package Zettel8;

class A {
    A() {
        System.out.print("1");
    }
    void call() {
        System.out.print("2");
        f().f();
    }
    A f() {
        System.out.print("3");
        return new B();
    }
}
class B extends A {
    A f() {
        System.out.print("4");
        return new A();
    }
}
public class Polymorphie {
    public static void main(String[] args) {
        System.out.print ("5");
        A obj = new B ( );
        System.out.print ("6");
        obj.call ( );
        System.out.print ("7");
    }}
