package Zettel9;
import java.util.*;
import java.io.*;
class Person implements Serializable{
	private String firstname;
	private String lastname;
	private String sortname;
	private static final long serialVersionUID = 1L;

	public Person() {
	}


	public Person(String firstname, String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
		updateSortname ( );
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
		updateSortname ( );
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
		updateSortname ( );
	}

	public String getSortname() {
		return sortname;
	}

	public void updateSortname() {
		sortname = lastname + firstname;
	}

	@Override
	public String toString() {
		return firstname + " " + lastname + " (" + sortname + ")";
	}

	public static List<Person> load(String filename) throws IOException {

		List<Person> list = new ArrayList<> ( );
		File f = new File (filename);

		if (f.exists ( )) {
			// in wird bei Verlassen des try-Blocks automatische geschlossen
			try (DataInputStream in = new DataInputStream (            // Lesen von bytecodierten primitven Datentypen + Strings
					new BufferedInputStream (        // Optionaler Buffer für die Performance
							new FileInputStream (f)    // Lesen von Bytes aus einer Datei
					))) {
				while (0 < in.available ( )) {        // Solange es noch nicht gelesene Bytes gibt
					Person d = load (in);    // Lese daraus ein Data-Objekt
					list.add (d);                // und füge es in die Liste ein
				}
			}

			return list;
		}
		return null;
	}


	public static Person load(DataInputStream in) throws IOException {
		Person d = new Person (in.readUTF ( ), in.readUTF ( ));
		return d;
	}

	public static void save(String filename, List<Person> list) throws IOException {

			// out wird bei Verlassen des try-Blocks automatische geschlossen
		try (DataOutputStream out = new DataOutputStream (            // Bytecodierte Schreiben von primitven Datentypen + Strings
				new BufferedOutputStream (        // Optionaler Buffer für die Performance
						new FileOutputStream (filename)    // Schreiben von Bytes in eine Datei
				))) {
			for (Person p : list) {
				save (out, p);
			}
		}
	}

	public static void save(DataOutputStream out, Person person) throws IOException {
		out.writeUTF (person.getFirstname ( ));
		out.writeUTF (person.getLastname ( ));

	}

	public static List<Person> unserialize(String filename) throws IOException, ClassNotFoundException {
		Person persons = null;
		ObjectInputStream in = null;
		ArrayList<Person> list = null;
		try {
			in = new ObjectInputStream (new BufferedInputStream (new FileInputStream (filename)));

				list =((ArrayList<Person>) in.readObject());

		} finally {
			if (in != null) {
				in.close ( );
			}
		}
		return list;
	}


	public static void serialize(String filename, List<Person> persons) throws IOException {
		ObjectOutputStream out = null;

		try {
			out = new ObjectOutputStream (new BufferedOutputStream (new FileOutputStream (filename)));
				out.writeObject (persons);

		} finally {
			if (out != null) {
				out.close ( );
			}
		}
	}
}

	public class PersonTest {

		public static void main(String[] args) throws IOException, ClassNotFoundException {
			List<Person> persons = new ArrayList<> ( );
			persons.add (new Person ("Willy", "Wonka"));
			persons.add (new Person ("Charlie", "Bucket"));
			persons.add (new Person ("Grandpa", "Joe"));
			System.out.println (persons);

			Person.save ("persons.ser", persons);
			persons = Person.load ("persons.ser");
			System.out.println (persons);
			Person.serialize ("persons2.ser", persons);
			persons = Person.unserialize ("persons2.ser");
			System.out.println (persons);
		}

	}

