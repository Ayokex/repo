package Zettel9Tut;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

// Anspruchsvolleres Beispiel zur manuellen Serialisierung eines Objekts
public class Data {
	public static void main(String[] args) throws Exception {
		List<Data> outData = new ArrayList<>(), inData;
		
		// Beispielliste erzeugen
		outData.add(new Data("A", new int[] {1,2,3}, null));
		outData.add(
			new Data("B1", new int[] {}, 
				new Data("B2", new int[] {4,5},
						new Data("B3", new int[] {6}, null)
				)
			)
		);	
		outData.add(outData.get(0));	// Doppelte Referenz auf ein Objekt
		
		// Liste in eine Datei schreiben
		save("test.data", outData);
		
		print("Geschrieben:", outData);
		
		
		// Liste wieder aus der Datei lesen
		inData = load("test.data");
		
		print("Gelesen:", inData);
		
		System.out.println("\ninData.equals(outData) => " + inData.equals(outData));
	}

	// Schreibt die Liste mit Data-Objekten in die Datei mit gegebenen Namen
	public static void save(String filename, List<Data> datas) throws FileNotFoundException, IOException {
		
		// IDs der bereits geschriebenen Objekte (damit die Objektidentität erhalten bleibt)
		Set<Integer> written = new HashSet<>();
		
		// out wird bei Verlassen des try-Blocks automatische geschlossen
		try(DataOutputStream out = 	new DataOutputStream(			// Bytecodierte Schreiben von primitven Datentypen + Strings
									new BufferedOutputStream(		// Optionaler Buffer für die Performance
									new FileOutputStream(filename)	// Schreiben von Bytes in eine Datei
		))) {
			for(Data data : datas) {
				save(out, data, written);
			}
		}
	}
	
	// Schreibt ein Data-Objekt in den gegebenen Stream
	public static void save(DataOutputStream out, Data data, Set<Integer> written) throws IOException {
		
		// Objektidentität durch hashCode. Object::hashCode liefert die Adresse im Speicher => eindeutig
		Integer objID = data.hashCode();
		
		// Neues oder bereits geschriebenes Objekt?
		boolean ref  = written.contains(objID);

		out.writeBoolean(ref);
		out.writeInt(objID);
		
		// Bei einem neuen Objekt folgen die serialisierten Attribute
		if(!ref) {
			
			written.add(objID); // Markiere data als geschrieben (muss vor dem rekrusiven Aufruf von save geschehen)
			
			out.writeUTF(data.name);			// Name in modifizierter UTF8-Codierung
			
			out.writeInt(data.nummern.length);	// Länge von nummmern, notwendig zur Rekonstruktion
			
			for(int i : data.nummern) {
				out.writeInt(i);				// Einzelne Einträge (nach aufsteigendem Index)
			}
			
			boolean hasRelevant = data.relevant != null;
			
			out.writeBoolean(hasRelevant);		// Folgt eine Referenz auf ein Data-Objekt?
			
			if(hasRelevant) {
				save(out, data.relevant, written);
			} 
		}
	}
	
	// Rekonstruiert eine Liste von Data-Objekten aus der Datei mit gegebenen Namen
	public static List<Data> load(String filename) throws FileNotFoundException, IOException {
		
		List<Data> datas = new ArrayList<>();
		
		// Speicher für die bereits gelesene Objekte zugreifbar über ihre ID
		Map<Integer, Data> loaded = new HashMap<>();
		
		// in wird bei Verlassen des try-Blocks automatische geschlossen
		try(DataInputStream in = 	new DataInputStream(			// Lesen von bytecodierten primitven Datentypen + Strings
									new BufferedInputStream(		// Optionaler Buffer für die Performance
									new FileInputStream(filename)	// Lesen von Bytes aus einer Datei
		))) {
			while(0 < in.available()) {		// Solange es noch nicht gelesene Bytes gibt
				Data d = load(in, loaded);	// Lese daraus ein Data-Objekt
				datas.add(d);				// und füge es in die Liste ein
			}
		}
		
		return datas;
	}
	
	// Liest ein Data-Objekt aus dem gegebenen Stream (analog zum Schreiben)
	public static Data load(DataInputStream in, Map<Integer, Data> loaded) throws IOException {
		
		boolean reference = in.readBoolean();	// Neues oder bestehendes Objekt?
		int objId = in.readInt();				// Eindeutige ID
		
		if(reference) {					// Objekt wurde bereits gelesen,
			return loaded.get(objId);	// also steht es in der Map
		} 
		
		else {
			Data d = new Data();		// Ein neues Element muss gelesen und
			loaded.put(objId, d);		// in die Map eingefügt werden
			
			d.name = in.readUTF();		// Name in modifizierter UTF8-Codierung
			
			int len = in.readInt();		// Länge von nummmern, notwendig zum
			d.nummern = new int[len];	// Erzeugen und Lesen des Ararys
			
			for(int i = 0; i < len; i++) {
				d.nummern[i] = in.readInt();	// Einzelne Einträge mit aufsteigendem Index lesen
			}
			
			if(in.readBoolean()) {				// Wenn eine Referenz auf ein Data-Objekt folgt,
				d.relevant = load(in, loaded);	// dann Lese dieses rekursiv und setze die Referenz entsprechend
			}
			
			return d;
		}
	}
	
	// Zu schreibende Daten
	private String name;
	private int[] nummern;
	private Data relevant;

	public Data(String name, int[] nummern, Data relevant) {
		this.name = name;
		this.nummern = nummern;
		this.relevant = relevant;
	}
	
	public Data() {
		this(null, null, null);
	}

	public void print() {
		print(0);
	}
	
	public void print(int depth) {
		for(int i = 0; i < depth; i++) {
			System.out.print('\t');
		}
		System.out.printf("%s: %s\n", name, Arrays.toString(nummern));
		
		if(this.relevant != null) {
			this.relevant.print(depth + 1);
		}
	}

	// Vergleiche zwei Objekte auf logische Äquivalenz
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || !(obj instanceof Data)) return false;

		Data other = (Data) obj;
		
		return Objects.equals(name, other.name)
				&& Arrays.equals(nummern, other.nummern)
				&& Objects.equals(relevant, other.relevant);
	}
	
	// Ausgabe mit Test auf Gleichheit der Objekte an Index 0 und 2
	private static void print(String msg, List<Data> datas) {
		System.out.println(msg);
		
		for(Data d : datas) d.print();
		
		if(datas.size() >= 2) System.out.println("datas[0] == datas[2] => " + (datas.get(0) == datas.get(2)));
		
		System.out.println();
	}
}
