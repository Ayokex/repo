package Zettel9Tut;

import java.io.IOException;

// Kleines Beispiel zu Exceptions
public class Exceptions {

	public static void main(String[] args) {
		try { 
			stuff(); 
		} 
		
		catch(IOException e) {
			System.out.println(e.getMessage());
		} 
	}
	
	static void stuff() throws IOException {
		// Münzwurf über den Typ der Exception
		if(Math.random() < 0.5) {
			
			// IOException wird in dem catch-Block in main behandelt
			throw new IOException("IO");
		} else {		
			
			// NullPointerException wird ignoriert, da sie keine Unterklasse von IOException ist
			throw new NullPointerException("NPE");
		}
	}
}
