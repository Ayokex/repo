package Zettel9Tut;

import java.util.ArrayList;
import java.util.List;

// Vorgabe
interface Older<T> {
	public boolean isOlder(T other);
}

//Vorgabe
public class TestGroup {

	public static void main(String[] args) {
		Group<Person> group = new Group<>();
		group.add(new Person("Alice", 25));
		group.add(new Person("Bob", 23));
		group.add(new Person("Carl", 26));
		System.out.println(group.getOldest().getName());
	}
}

class Person implements Older<Person> {	// Person kann mit einer anderen Person bzgl. ihres Alters verglichen werden

	// Impliziete Vorgabe durch das TestProgramm
	private String name;
	private int alter;
	
	public Person(String n, int a) {
		this.name = n;
		this.alter = a;
	}
	
	public String getName() { return name; }

	// Beim Überschreiben wird hier der generische Paramater T durch Person ersetzt
	@Override
	public boolean isOlder(Person other) {
		return this.alter < other.alter;
	}
}

// Gruppe enthält Mitglieder, die bezüglich ihres Alters verglichen werden können.
// Für das Testprogramm hätte auch Person funktioniert, so ist die Klasse aber flexibler einsetzbar
class Group<T extends Older<T>> {

	// Irgendeine Datenstruktur für die Mitglieder
	private List<T> members = new ArrayList<>();
	
	public void add(T member) {
		members.add(member);
	}

	// Liefert das älteste Mitglied in der Gruppe
	public T getOldest() {
		T oldest = members.get(0);
		
		for(int i = 1; i < members.size(); i++) {
			T cur = members.get(i);
			
			// Der Typparameter T muss auf Klassen beschränkt werden, die
			// Older implementieren, damit die Methode isOlder verfügbar ist
			if(oldest.isOlder(cur)) {
				oldest = cur;
			}
		}
		
		return oldest;
	}

}